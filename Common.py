#############################################################################################################################
from socket import *
# Connect to a listening socket.
def Connect(Host, Port):
    Sock = socket(AF_INET, SOCK_STREAM)
    Sock.connect((Host, Port))
    return Sock
# Receive data from a socket.
# Pieces of data are separated with !, message is ended with #.
# Returns a list of strings.
def Receive(Sock):
    x = ""
    while True:
        y = Sock.recv(1024).split("#")
        x += y[0]
        if len(y) == 2:
            break
    z = x.split("!")
    if z == [""]:
      return []
    else:
      return z
# Takes a list of strings, sends it.
def Send(Sock, x):
    Sock.sendall("!".join(x) + "#")