#############################################################################################################################
from Common import *
from argparse import *
from socket import *
from threading import *
global Clients
global Ongoing
global Open
# Checks if a user name is already picked.
def Duplicate(Name):
  for (Existing_name, _) in Clients:
    if Name == Existing_name:
      return True
  return False
# Waits for the client to create or join a game.
def Listen(Name, Sock):
  global Open
  Action = Receive(Sock)
  if Action[0] == "c":
    Open += [(int(Action[1]), int(Action[2]), [(Name, Sock, Action[4].split("$"))], int(Action[3]))]
  elif Action[0] == "j":
    Send(Sock, ["$".join(map(str, [Height, Width, len(Players), Goal])) for (Height, Width, Players, Goal) in Open])
if __name__ == "__main__":
  global Clients
  global Ongoing
  global Open
  Clients = []
  Ongoing = []
  Open = []
  parser = ArgumentParser()
  parser.add_argument("-H", "--host", default = "127.0.0.1", help = "Address", type = str)
  parser.add_argument("-P", "--port", default = 7777, help = "Port", type = int)
  args = parser.parse_args()
  My_sock = socket(AF_INET, SOCK_STREAM)
  My_sock.bind((args.host, args.port))
  My_sock.listen(5)
  while True:
    (Client_sock, _) = My_sock.accept()
    [Name] = Receive(Client_sock)
    if Duplicate(Name):
      Send(Client_sock, ["0"])
      Client_sock.close()
    else:
      Clients += [(Name, Client_sock)]
      Send(Client_sock, ["1"])
      Thread(target = Listen, args=(Name, Client_sock)).start()