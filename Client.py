#############################################################################################################################
from Common import *
from argparse import *
from ast import *
from socket import *
# Appends y to x[i] where x is a tuple of lists.
def Add(x, i, y):
  z = list(x)
  z[i] += [y]
  return tuple(z)
# Height, Width - dimensions of the grid.
# Grid - game board.
# Ships - tuple of lists of coordinates of existing ships (ships are placed into different lists depending on their length).
# Action - "p" for place or "r" for remove.
# Coordinates - end point coordinates to add or remove a ship.
# Len - the lenght of the ship minus two (for indexing in the tuple of lists of ships).
# Celles - a list of cells to add or remove a ship.
def Add_ship(Height, Width, Grid, Ships, Action, Coordinates, Len, Cells):
  if Action == "p":
    if Len > -1 and Len < 4:
      if len(Ships[Len]) == 4 - Len:
        print "\nShips of this size have already all been placed.\n"
      else:
        Free = True
        for (i, j) in Cells:
          if Grid[i][j] != " ":
            Free = False
            print "\nThose cells are not free.\n"
            break
        if Free:
          for (i, j) in Cells:
            Grid[i][j] = "O"
          Draw_grid(Height, Width, Grid)
          return Read_ships_2(Height, Width, Grid, Add(Ships, Len, (Coordinates, Cells)))
    else:
      print "\nShips of this length are not allowed.\n"
  elif Action == "r":
    for i in range(len(Ships)):
      if Ships[i][0] == Coordinates:
        for (j, k) in Ships[i][1]:
          Grid[j][k] == " "
          Draw_grid(Height, Width, Grid)
        return Read_ships_2(Height, Width, Grid, Rem(Ships, Len, i))
    print "\nNo ship found at those coordinates.\n"
  return Read_ships_2(Height, Width, Grid, Ships)
# Checks if x is in range(n)
def Coordinate(n, x):
  return x > -1 and x < n
# Draws the grid.
# Ships are denoted by O, hits by X.
def Draw_grid(Height, Width, Grid):
  print Sandwich(Sandwich("+", Width * "-") + "\n", Join([Sandwich("|", Join(Grid[i])) + "\n" for i in range(Height)]))
# Takes a list of coordinates and check if they are valid coordinates for an m x n grid.
def In_grid(m, n, x):
  if x == []:
    return True
  else:
    return Coordinate(m, x[0][0]) and Coordinate(n, x[0][1]) < n and In_grid(m, n, x[1 :])
# Checks if a list consists of integers.
def Ints(x):
  if x == []:
    return True
  else:
    return isinstance(x[0], int) and Ints(x[1 :])
# Concatenate a list of strings.
def Join(x):
  return "".join(x)
# 
def Read_action(Sock):
  Action = raw_input()
  if Action == "c":
    print "\nEnter the dimensions of the board as (height, width)."
    print "Keep in mind that the board should be large enough for the following ships:"
    print "4 ships of size 2"
    print "3 ships of size 3"
    print "2 ships of size 4"
    print "1 ship of size 5\n"
    Read_dimensions(Sock)
  elif Action == "j":
    Send(Sock, ["j"])
    Games = Receive(Sock)
    if Games == []:
      print "\nThere are no games currently available."
      print "You can create your own game or wait for a game to become available and join.\n"
    else:
      for (Height, Width, Players, Goal) in Games:
        print "grid dimensions (" + Height + ", " + Width + "), players needed " + Players + ", players connected " + Goal
      # TODO game id-s
      print "\nIf you want to join any of those games, input game ID.\n"
  else:
    print "\nThis is not a valid option.\n"
    Read_action(Sock)
# 
def Read_dimensions(Sock):
  try:
    x = literal_eval(raw_input())
    if isinstance(x, tuple):
      if len(x) == 2:
        if isinstance(x[0], int) and isinstance(x[1], int):
          if x[0] < 1 or x[1] < 1:
            print "\nDimensions should be positive.\n"
            Read_dimensions(Sock)
          else:
            if x[1] >= 30 / x[0]:
              print "\nEnter the number of players.\n"
              Read_players(x[0], x[1], Sock)
            else:
              print "\nThis grid is too small to accommodate all the ships.\n"
              Read_dimensions(Sock)
        else:
          print "\nDimensions should be integers.\n"
          Read_dimensions(Sock)
      else:
        print "\nThe grid has two dimensions.\n"
        Read_dimensions(Sock)
    else:
      print "\nIncorrect formatting."
      Read_dimensions(Sock)
  except:
    print "\nIncorrect formatting."
    Read_dimensions(Sock)
# Asks a name from the user, checks with the server if it is in use, tries again if it is.
def Read_name(Host, Port):
  Sock = Connect(Host, Port)
  Send(Sock, [raw_input()])
  [y] = Receive(Sock)
  if y == "0":
    Sock.close()
    print "\nThis name already exists.\n"
    Read_name(Host, Port)
  elif y == "1":
    print "\nDo you want to join a game or create a game? Write \"c\" or \"j\".\n"
    Read_action(Sock)
# Asks the number of players from the user, checks if it is a valid number, tries again if not.
def Read_players(Height, Width, Sock):
  x = raw_input()
  try:
    y = int(x)
    if y < 2:
      print "\nThere should be no less than two players."
      Read_players(Height, Width, Sock)
    else:
      Grid = Read_ships(Height, Width)
      GridStr = "$".join(map(Join, Grid))
      Send(Sock, ["c", str(Height), str(Width), x, GridStr])
  except:
    print "\nThe number of players should be an integer.\n"
    Read_players(Height, Width, Sock)
# Asks the player to place their ships.
# Actual work is done by Read_ships_2, this is just for giving instructions and constructing an empty grid.
def Read_ships(Height, Width):
  print "\nWrite ship placements as coordinates of end points: p((x1, y1), (x2, y2))."
  print "For removing a ship, write r((x1, y1), (x2, y2))."
  print "Coordinates start from zero!"
  print "To confirm ship placement, write \"c\".\n"
  Grid = [[" " for j in range(Width)] for i in range(Height)]
  Draw_grid(Height, Width, Grid)
  return Read_ships_2(Height, Width, Grid, ([], [], [], []))
# Reads user instructions for ship placement.
# Three possible actions: confirm, place, remove.
# A lot of input validity checks.
# 12 levels of indentation :P
def Read_ships_2(Height, Width, Grid, Ships):
  x = raw_input()
  if x[0] == "c":
    if len(x) == 1:
      if len(Ships[0]) == 4 and len(Ships[1]) == 3 and len(Ships[2]) == 2 and len(Ships[3]) == 1:
        return Grid
      else:
        print "\nYou haven't placed all ships yet.\n"
    else:
      print "\nConfirm action does not need any arguments.\n"
  elif x[0] == 'p' or x[0] == 'r':
    try:
      y = literal_eval(x[1 :])
      if isinstance(y, tuple):
        if len(y) == 2:
          if isinstance(y[0], tuple) and isinstance(y[1], tuple):
            if len(y[0]) == 2 and len(y[1]) == 2:
              if Ints([y[0][0], y[0][1], y[1][0], y[1][1]]):
                if In_grid(Height, Width, [y[0], y[1]]):
                  if y[0][0] == y[1][0]:
                    if y[0][1] > y[1][1]:
                      z = (y[1], y[0])
                    else:
                      z = y
                    Len = z[1][1] - z[0][1] - 1
                    Cells = [(y[0][0], i) for i in range(z[0][1], z[1][1] + 1)]
                    return Add_ship(Height, Width, Grid, Ships, x[0], z, Len, Cells)
                  else:
                    if y[0][1] == y[1][1]:
                      if y[0][0] > y[1][0]:
                        z = (y[1], y[0])
                      else:
                        z = y
                      Len = z[1][0] - z[0][0] - 1
                      Cells = [(i, z[0][1]) for i in range(z[0][0], z[1][0] + 1)]
                      return Add_ship(Height, Width, Grid, Ships, x[0], z, Len, Cells)
                    else:
                      print "\nShips can not be placed diagonally.\n"
                else:
                  print "\nShip coordinates outside of grid.\n"
              else:
                print "\nShip coordinates should be integers.\n"
            else:
              print "\nEach end point should be specified by two coordinates.\n"
          else:
            print "\nIncorrect formatting.\n"
        else:
          print "\nA ship should be specified by two end points.\n"
      else:
        print "\nIncorrect formatting.\n"
    except:
      print "\nIncorrect formatting.\n"
  return Read_ships_2(Height, Width, Grid, Ships)
# Removes x[i][j] from x, where x is a tuple of lists.
def Rem(x, i, j):
  y = list(x)
  y[i] = y[i][: j] + y[i][j + 1 :]
  return tuple(y)
# For concatenating pieces of the border with the grid when drawing the board.
def Sandwich(x, y):
  return x + y + x
if __name__ == "__main__":
  Parser = ArgumentParser()
  Parser.add_argument("-H", "--Host", default = "127.0.0.1", help = "Server address", type = str)
  Parser.add_argument("-P", "--Port", default = 7777, help = "Server port", type = int)
  Args = Parser.parse_args()
  print "\nPick a name\n"
  Read_name(Args.Host, Args.Port)